package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 6/10/17.
 */

import org.apache.commons.codec.binary.Base64;

import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AES {

    private final String ALGO = "AES";
    private final String defaultKeyStr = "nosecretkeyyet";
    private String keyStr = "nosecretkeyyet";
    private static AES instance = null;

    private AES() {}

    public static AES getInstace () {
        if (instance == null)
            instance = new AES();
        return instance;
    }

    public String getKeyPhrase () { return keyStr; }
    public void setDefaultKeyPhrase () { keyStr = defaultKeyStr; }
    public void setKeyPhrase (String keyPhrase) {
        keyStr = keyPhrase;
    }

    private Key generateKey() throws Exception {
        byte[] keyValue = keyStr.getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        keyValue = sha.digest(keyValue);
        keyValue = Arrays.copyOf(keyValue, 16); // use only first 128 bit
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }

    public String encrypt(String Data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        //String encryptedValue = DatatypeConverter.printBase64Binary(encVal);
        String encryptedValue = new String (Base64.encodeBase64(encVal), "UTF-8");
        return encryptedValue;
    }

    public String decrypt(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        //byte[] decordedValue = DatatypeConverter.parseBase64Binary(encryptedData);
        byte[] decodedValue = Base64.decodeBase64 (encryptedData.getBytes());
        byte[] decValue = c.doFinal(decodedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    public byte[] encrypt (byte[] contents) throws Exception {
        Key key = generateKey ();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal (contents);
        return (Base64.encodeBase64(encVal));
    }

    public byte[] decrypt (byte[] contents) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        //byte[] decordedValue = DatatypeConverter.parseBase64Binary(encryptedData);
        byte[] decodedValue = Base64.decodeBase64 (contents);
        byte[] decValue = c.doFinal(decodedValue);
        return decValue;
    }
}
