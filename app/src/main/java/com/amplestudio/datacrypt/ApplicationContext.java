package com.amplestudio.datacrypt;

import android.content.Context;

/**
 * Created by myatphyothu on 27/9/17.
 */

public class ApplicationContext {

    private Context appContext = null;

    private ApplicationContext(){}

    public void init(Context context){
        if(appContext == null){
            appContext = context;
        }
    }

    private Context getContext(){
        return appContext;
    }

    public static Context get(){
        return getInstance().getContext();
    }

    private static ApplicationContext instance;

    public static ApplicationContext getInstance(){
        return instance == null ?
                (instance = new ApplicationContext()):
                instance;
    }
}

