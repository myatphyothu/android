package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 22/10/17.
 */

public class CorporateProfile {
    public String mName = "";
    public String mURL = "";
    public String mPort = "";
    public String mUsername = "";
    public String mPassword = "";
    public String mLocation = "";
    public String mVicinity = ""; // in km
    public boolean exists = false;
    public boolean locationDataUpdated = false;
    public boolean downloadCorporateLocation = false;
    public static final String CORPORATE_NAME = "NAME";
    public static final String CORPORATE_IP_ADDRESS = "IP_ADDRESS";
    public static final String CORPORATE_PORT = "PORT";
    public static final String CORPORATE_USERNAME = "USERNAME";
    public static final String CORPORATE_PASSWORD = "PASSWORD";
    public static final String CORPORATE_LOCATION = "LOCATION";
    public static final String CORPORATE_VICINITY = "VICINITY";

    static final int index_Name = 1;
    static final int index_IPAddress = 2;
    static final int index_Port = 3;
    static final int index_Username = 4;
    static final int index_Password = 5;
    static final int index_Location = 6;
    static final int index_Vicinity = 7;

    public static final String msg_LoggedOn = "Online";
    public static final String msg_LoggedOff = "Offline";

    public static boolean isLoggedOn = false;
    public static boolean promptLogin = false;


    private static CorporateProfile instance = null;


    private CorporateProfile () {

    }

    public static CorporateProfile getInstance () {

        if (instance == null)
            instance = new CorporateProfile();
        return instance;
    }

    public void set (String _name, String _url, String _port, String _username, String _password, String _location, String _vicinity) {
        mName = _name;
        mURL = _url;
        mPort = _port;
        mUsername = _username;
        mPassword = _password;
        mLocation = _location;
        mVicinity = _vicinity;
    }


    public String getLatitude () {
        return mLocation.split(" ")[0];
    }

    public String getLongitude () {
        return mLocation.split(" ")[1];
    }
}
