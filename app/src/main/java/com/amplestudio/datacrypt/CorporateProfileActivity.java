package com.amplestudio.datacrypt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

public class CorporateProfileActivity extends AppCompatActivity {

    EditText mEditText_CorporateName;
    EditText mEditText_FTPURL;
    EditText mEditText_FTPPort;
    EditText mEditText_FTPUsername;
    EditText mEditText_FTPPassword;
    TextView mTextView_CorporateActivityHeading;
    Button mButton_Next;
    Button mButton_Cancel;
    Button mButton_Delete;
    private boolean mProfileStatus = false;
    private String mExistingCorporateName = "";


    private static final Pattern IP_ADDRESS
            = Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))");


    private void receiveIntent () {
        Intent receivedIntent = getIntent();
        Bundle extras = receivedIntent.getExtras();
        if (extras == null)
            return;

        mProfileStatus = extras.getBoolean ("ProfileStatus");
        UIController.getInstance().init (this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporate_profile);
        receiveIntent ();

        initUIElements ();
    }

    @Override
    public void onBackPressed() {
    }

    private void populateUIFields () {
        CorporateProfile corporateProfile = CorporateProfile.getInstance();
        if (corporateProfile != null) {
            mEditText_CorporateName.setText (corporateProfile.mName);
            mEditText_FTPURL.setText (corporateProfile.mURL);
            mEditText_FTPPort.setText (corporateProfile.mPort);
            mEditText_FTPUsername.setText (corporateProfile.mUsername);
            mExistingCorporateName = mEditText_CorporateName.getText().toString();
        }
    }

    private void initUIElements () {
        mEditText_CorporateName = (EditText) findViewById (R.id.editText_corporate_name);
        mEditText_FTPURL = (EditText) findViewById (R.id.editText_ftp_url);
        mEditText_FTPPort = (EditText) findViewById (R.id.editText_ftp_port);
        mEditText_FTPUsername = (EditText) findViewById (R.id.editText_ftp_username);
        mEditText_FTPPassword = (EditText) findViewById (R.id.editText_ftp_password);
        mButton_Next = (Button) findViewById (R.id.button_next);
        mButton_Cancel = (Button) findViewById (R.id.button_cancel);
        mButton_Delete = (Button) findViewById (R.id.button_delete);
        mTextView_CorporateActivityHeading = (TextView) findViewById(R.id.textview_CorporateActivityHeading);

        // if profile is existing, populate respective fields with the values from db
        if (mProfileStatus) {
            mButton_Delete.setVisibility(View.VISIBLE);
            mButton_Cancel.setVisibility(View.VISIBLE);

            mButton_Next.setText ("Update");
            mTextView_CorporateActivityHeading.setText ("Corporate Profile Edit");
            populateUIFields();

        } else {
            mButton_Delete.setVisibility(View.INVISIBLE);
            mButton_Cancel.setVisibility(View.INVISIBLE);
            mButton_Next.setText ("Setup");
            mTextView_CorporateActivityHeading.setText ("Corporate Profile Setup");
        }

        mButton_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean validFields = validFields();
                if (validFields) {
                    String name = mEditText_CorporateName.getText().toString ();
                    final String url = mEditText_FTPURL.getText().toString ();
                    final String port = mEditText_FTPPort.getText().toString ();
                    final String username = mEditText_FTPUsername.getText().toString ();
                    final String password = mEditText_FTPPassword.getText().toString ();
                    String location = "0,0";
                    String vicinity = "2";


                    // insert into database
                    boolean insertionStatus = DatabaseController.getInstance().insertIntoCorporateProfileTable(mExistingCorporateName, name, url, port, username, password, location, vicinity);
                    if (insertionStatus) {
                        UIController.getInstance().alert ("Corporate Profile",
                                mProfileStatus ? "Profile update successful" : "Profile creation successful", R.drawable.ic_success);
                        CorporateProfile.getInstance().set(name, url, port, username, password, location, vicinity);


                    } else {
                        UIController.getInstance().alert ("Corporate Profile",
                                mProfileStatus ? "Profile update failure" : "Profile creation failure", R.drawable.ic_error);
                    }
                }
                //validateFields();
                UIController.getInstance().startMainActivity();
            }
        });

        mButton_Cancel.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish ();
            }
        });

        mButton_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean delete = DatabaseController.getInstance().deleteCorporateProfile (mExistingCorporateName);
                if (delete) {
                    UIController.getInstance().alert ("Corporate Profile", "Profile deleted", R.drawable.ic_success);
                    UIController.getInstance().startMainActivity();
                    //finish ();
                }


            }
        });
    }

    private boolean validFields () {
        boolean result = false;

        if (!validCorporateName()) {
            return result;
        }
        if (!validFTPURL()) {
            return result;
        }
        if (!validFTPPort()) {
            return result;
        }
        if (!validFTPUsername()) {
            return result;
        }
        if (!validFTPPassword()) {
            return result;
        }
        result = true;

        return result;
    }

    private boolean validCorporateName () {
        boolean result = true;
        if (mEditText_CorporateName.getText().toString().isEmpty()) {
            UIController.getInstance().alert("Error", "Empty value in Corporate Name", R.drawable.ic_error);
            result = false;
        }
        return result;
    }

    private boolean validFTPURL () {
        boolean result = true;
        if (mEditText_FTPURL.getText().toString().isEmpty()) {
            UIController.getInstance().alert("Error", "Empty value in FTP URL", R.drawable.ic_error);
            result = false;
        }
        /*
        String URLValue = mEditText_FTPURL.getText().toString();

        Matcher matcher = IP_ADDRESS.matcher(URLValue);
        if (!matcher.matches()) {
            UIController.getInstance().alert("Error", "Invalid IP Address", R.drawable.ic_error);
            result = false;
        }
        */
        return result;
    }

    private boolean validFTPPort () {
        boolean result = true;
        if (mEditText_FTPPort.getText().toString().isEmpty()) {
            UIController.getInstance().alert("Error", "Empty value in FTP Port", R.drawable.ic_error);
            result = false;
            return result;
        }

        String port = mEditText_FTPPort.getText().toString();

        try {
            int portValue = Integer.parseInt (port);
        } catch (Exception e) {
            UIController.getInstance().alert ("Error", "Invalid FTP Port value", R.drawable.ic_error);
            result = false;
        }
        return result;
    }

    private boolean validFTPUsername () {
        boolean result = true;
        if (mEditText_FTPUsername.getText().toString().isEmpty()) {
            UIController.getInstance().alert("Error", "Empty value in FTP Username", R.drawable.ic_error);
            result = false;
        }
        return result;
    }

    private boolean validFTPPassword () {
        boolean result = true;
        if (mEditText_FTPPassword.getText().toString().isEmpty()) {
            UIController.getInstance().alert("Error", "Empty value in FTP Password", R.drawable.ic_error);
            result = false;
        }
        return result;
    }

    private void showCorporateProfleData () {
        CorporateProfile corporateProfile =  CorporateProfile.getInstance();
        if (corporateProfile != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append ("Name: " + corporateProfile.mName + "\n");
            stringBuffer.append ("URL: " + corporateProfile.mURL + "\n");
            stringBuffer.append ("Port: " + corporateProfile.mPort + "\n");
            stringBuffer.append ("Username: " + corporateProfile.mUsername + "\n");
            stringBuffer.append ("Password: " + corporateProfile.mPassword + "\n");
            stringBuffer.append ("Location: " + corporateProfile.mLocation + "\n");

            builder.setCancelable(true);
            builder.setTitle("Corporate Profile");
            builder.setMessage(stringBuffer.toString());
            builder.show();
        }
    }
}
