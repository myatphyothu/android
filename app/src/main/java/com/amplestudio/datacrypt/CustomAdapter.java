package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 23/9/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class CustomAdapter extends BaseAdapter {

    String[] names;
    String[] descriptions;
    Context context;

    ArrayList<DataObject> m_DataObjects = null;
    int[] imageID;

    private static LayoutInflater inflater = null;

    public CustomAdapter(ArrayList<DataObject> _dataObjects) {
        super();
        m_DataObjects = new ArrayList<DataObject> (_dataObjects);
        context = ApplicationContext.get();
        inflater = (LayoutInflater) ApplicationContext.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // @Override
    public int getCount() {
        if (m_DataObjects == null) return 0;
        return m_DataObjects.size();
    }



    public void setData (ArrayList <DataObject> _dataObjects) {
        m_DataObjects.clear();
        m_DataObjects.addAll (_dataObjects);
    }

    public void remove (int position) {
        m_DataObjects.remove (position);
    }

    // @Override
    public Object getItem(int position) {
        return m_DataObjects.get(position);
    }


    // @Override
    public long getItemId (int position) {
        return position;
    }

    public class Holder {
        TextView row_item_filename;
        //TextView row_item_distanceToUser;
        ImageView row_item_icon;

    }



    // @Override
    public View getView (final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        DataObject dataObject = m_DataObjects.get(position);
        if (convertView == null) {
            holder = new Holder ();
            convertView = inflater.inflate (R.layout.row_item, null);

            holder.row_item_filename = (TextView) convertView.findViewById (R.id.row_item_filename);
            holder.row_item_icon = (ImageView) convertView.findViewById (R.id.row_item_icon);
            //holder.row_item_distanceToUser = (TextView) convertView.findViewById(R.id.row_item_distanceToUser);
            convertView.setTag (holder);
        } else {
            holder = (Holder) convertView.getTag ();

        }

        holder.row_item_filename.setText (dataObject.getFilename());

        /*
        if (dataObject.isEncrypted()) {
            holder.row_item_distanceToUser.setText(LocationService.getInstance().distanceToString(dataObject) + "km");

        } else {
            holder.row_item_distanceToUser.setText("");
        }
        */
            //holder.row_item_distanceToUser.setText (LocationService.getInstance().distanceToString(dataObject) + "km");

        /*
        if (dataObject.isEncrypted())
            holder.row_item_icon.setImageResource (R.drawable.ic_encrypted);
        else
            holder.row_item_icon.setImageResource (dataObject.GetImageID());*/

        holder.row_item_icon.setImageResource (dataObject.GetImageID());


        return convertView;
    }


}
