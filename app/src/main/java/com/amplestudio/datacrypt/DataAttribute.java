package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 25/9/17.
 */

public class DataAttribute {
    public String m_ColumnName;
    public String m_ColumnType;

    DataAttribute (String _ColumnName, String _ColumnType) {
        m_ColumnName = _ColumnName;
        m_ColumnType = _ColumnType;
    }
}
