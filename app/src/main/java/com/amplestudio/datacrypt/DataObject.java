package com.amplestudio.datacrypt;

import android.webkit.MimeTypeMap;

import java.io.File;

/**
 * Created by myatphyothu on 23/9/17.
 */

public class DataObject {
    File m_File;
    boolean m_Encrypted = false;
    FILE_TYPE m_FileType;
    int m_ImageID;
    String m_filename, m_location; // m_location: longitude and latitude
    String m_fileExtension;
    String m_password;
    String m_decryptedFilename;
    String m_mimeType;

    static final String tag_Filename = "FILE_NAME";
    static final String tag_FileType = "FILE_TYPE";
    static final String tag_FileStatus = "FILE_STATUS";
    static final String tag_UserLocation = "USER_LOCATION";
    static final String tag_FilePassword = "FILE_PWD";

    static final int index_Filename = 1;
    static final int index_FileType = 2;
    static final int index_FileStatus = 3;
    static final int index_UserLocation = 4;
    static final int index_FilePassword = 5;
    /*
    FILE_TYPE_TEXT,
    FILE_TYPE_PDF,
    FILE_TYPE_IMAGE,
    FILE_TYPE_AUDIO,
    FILE_TYPE_VID,
    FILE_TYPE_BINARY,
    FILE_TYPE_ERROR
     */
    public DataObject (File _file) {
        m_File = _file;
        ComputeFileMetaData();
    }


    public DataObject (File _file, String location) {
        m_File = _file;
        m_location = location;
        // get file metadata
        ComputeFileMetaData ();
    }

    public DataObject (String _filename, String _filetype, String _filestatus, String _location, String _password) {
        m_filename = _filename;
        m_fileExtension = _filetype;
        m_password = _password;
        m_location = _location;
        m_Encrypted = _filestatus.equals("true") ? true : false;
        // determine if file exists
        m_FileType = Utility.GetFileExtension (m_fileExtension);
        m_ImageID = Utility.GetIcon (m_FileType);
    }

    public DataObject (DataObject _dataObject) {
        m_filename = _dataObject.getFilename();
        m_fileExtension = _dataObject.getFileExtension();
        m_password = _dataObject.getFilePassword();
        m_location = _dataObject.getEncryptedLocation();
        m_Encrypted = _dataObject.isEncrypted();
        m_FileType = Utility.GetFileExtension (m_fileExtension);
        m_ImageID = Utility.GetIcon (m_FileType);
    }

    // use by constructor with File argument
    private void ComputeFileMetaData () {
        //if (m_File == null) return;
        // get file extension
        m_filename = m_File.getName();
        String lastExtension = m_filename.substring (m_filename.lastIndexOf(".")+1);
        if (lastExtension.equals("crypto")) {
            m_Encrypted = true;
            m_decryptedFilename = m_filename.substring(0, m_filename.lastIndexOf("."));
            m_fileExtension = m_decryptedFilename.substring (m_decryptedFilename.lastIndexOf(".")+1);
            m_mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(m_fileExtension.toLowerCase());
        }
        m_FileType = Utility.GetFileExtension (m_fileExtension);

        // get icon
        m_ImageID = Utility.GetIcon (m_FileType);

    }



    public void setEncryptedLocation (String location) { m_location = location; }
    public int GetImageID () { return m_ImageID; }
    public boolean isEncrypted () { return m_Encrypted; }
    public void setPassword (String password) { m_password = password; }

    public String getFilename () { return m_filename; }
    public String getDecryptedFilename () { return m_decryptedFilename; }
    public String getMimeType () { return m_mimeType; }
    public String getFileExtension () { return m_fileExtension; }
    public String getFileStatus () { return (m_Encrypted?"true" : "false"); }
    public String getEncryptedLocation () { return m_location; }
    public String getFilePassword () { return m_password; }
}
