package com.amplestudio.datacrypt;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by myatphyothu on 25/9/17.
 */

public class DatabaseController {

    private static DatabaseController instance = null;
    Context m_Context = null;
    public static final String CorporateProfileTable = "corporateprofile";
    public static final String FileRecordTable = "filerecord";

    DatabaseHelper m_FileRecord_Db = null;

    private DatabaseController() {
        if (m_Context == null)
            m_Context = ApplicationContext.get();
    }

    public static DatabaseController getInstance() {
        if (instance == null) {
            instance = new DatabaseController();
        }
        return instance;
    }

    public void init() {
        if (m_FileRecord_Db == null) {
            m_FileRecord_Db = new DatabaseHelper(ApplicationContext.get());
            initFileRecordTable();
            initCorporateProfileTable();
        }
    }

    private void initFileRecordTable() {
        ArrayList<DataAttribute> columns = new ArrayList<DataAttribute>();
        columns.add(new DataAttribute("ID", "INTEGER"));
        columns.add(new DataAttribute("FILE_NAME", "TEXT"));
        columns.add(new DataAttribute("FILE_TYPE", "TEXT"));
        columns.add(new DataAttribute("FILE_STATUS", "TEXT"));
        columns.add(new DataAttribute("USER_LOCATION", "TEXT"));
        columns.add(new DataAttribute("FILE_PWD", "TEXT"));
        m_FileRecord_Db.addTable(FileRecordTable, columns);
        //m_FileRecord_Db = new DatabaseHelper(m_Context, FileRecordTable, columns);
    }

    private void initCorporateProfileTable() {

        ArrayList<DataAttribute> columns = new ArrayList<DataAttribute>();
        columns.add(new DataAttribute("ID", "INTEGER"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_NAME, "TEXT"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_IP_ADDRESS, "TEXT"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_PORT, "TEXT"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_USERNAME, "TEXT"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_PASSWORD, "TEXT"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_LOCATION, "TEXT"));
        columns.add(new DataAttribute(CorporateProfile.CORPORATE_VICINITY, "TEXT"));
        m_FileRecord_Db.addTable(CorporateProfileTable, columns);
        //m_CorporateProfile_Db = new DatabaseHelper (m_Context, CorporateProfileTable, columns);
    }

    public void updateDummyLocationData() {

        ArrayList<DataObject> dataObjects = getFileRecords();

        String[] dummyLatLng = LocationService.getInstance().getDummyLocations();
        int index = 0;
        for (DataObject dObj : dataObjects) {
            dObj.setEncryptedLocation(dummyLatLng[index]);
            updateFileRecord(dObj);
            ++index;
        }
    }

    public void retrieveCorporateProfile() {

        Cursor cursor = m_FileRecord_Db.getAllData(CorporateProfileTable);
        int total = cursor.getCount();
        if (total != 0) {
            int index = 0;

            while (cursor.moveToNext()) {
                String name = cursor.getString(CorporateProfile.index_Name);
                String url = cursor.getString(CorporateProfile.index_IPAddress);
                String port = cursor.getString(CorporateProfile.index_Port);
                String username = cursor.getString(CorporateProfile.index_Username);
                String password = cursor.getString(CorporateProfile.index_Password);
                String location = cursor.getString(CorporateProfile.index_Location);
                String vicinity = cursor.getString(CorporateProfile.index_Vicinity);

                CorporateProfile.getInstance().set (name, url, port, username, password, location, vicinity);
                CorporateProfile.getInstance().exists = true;
                break;
            }
        }

    }

    public boolean updateCorporateProfileTable () {
        CorporateProfile corporateProfile = CorporateProfile.getInstance();
        boolean result = false;
        HashMap<String, String> valuesToInsert = new HashMap<String, String>();

        valuesToInsert.put(CorporateProfile.CORPORATE_NAME, corporateProfile.mName);
        valuesToInsert.put(CorporateProfile.CORPORATE_IP_ADDRESS, corporateProfile.mURL);
        valuesToInsert.put(CorporateProfile.CORPORATE_PORT, corporateProfile.mPort);
        valuesToInsert.put(CorporateProfile.CORPORATE_USERNAME, corporateProfile.mUsername);
        valuesToInsert.put(CorporateProfile.CORPORATE_PASSWORD, corporateProfile.mPassword);
        valuesToInsert.put(CorporateProfile.CORPORATE_LOCATION, corporateProfile.mLocation);
        valuesToInsert.put(CorporateProfile.CORPORATE_VICINITY, corporateProfile.mVicinity);
        boolean tableExists = TableExists(CorporateProfileTable);

        if (tableExists) {
            boolean update = m_FileRecord_Db.updateData(CorporateProfileTable, CorporateProfile.CORPORATE_NAME, corporateProfile.mName, valuesToInsert);
            if (!update) {
                result = false;
                UIController.getInstance().alert("DatabaseController", "CorporateProfileActivity \nupdate failed", R.drawable.ic_error);
            } else {
                result = true;
            }
        }

        return result;
    }

    public boolean insertIntoCorporateProfileTable(String existing_name, String name, String url, String port, String username, String password, String location, String vicinity) {
        boolean result = false;
        HashMap<String, String> valuesToInsert = new HashMap<String, String>();

        valuesToInsert.put(CorporateProfile.CORPORATE_NAME, name);
        valuesToInsert.put(CorporateProfile.CORPORATE_IP_ADDRESS, url);
        valuesToInsert.put(CorporateProfile.CORPORATE_PORT, port);
        valuesToInsert.put(CorporateProfile.CORPORATE_USERNAME, username);
        valuesToInsert.put(CorporateProfile.CORPORATE_PASSWORD, password);
        valuesToInsert.put(CorporateProfile.CORPORATE_LOCATION, location);
        valuesToInsert.put(CorporateProfile.CORPORATE_VICINITY, vicinity);
        boolean tableExists = TableExists(CorporateProfileTable);

        if (!tableExists) {
            boolean createStatus = m_FileRecord_Db.createTable(CorporateProfileTable);
            if (!m_FileRecord_Db.TableExists(CorporateProfileTable)) {
                Log.d("Debug", "Table does not exist");
            } else {
                result = m_FileRecord_Db.insertData(CorporateProfileTable, valuesToInsert);
            }
        } else {
            if (TableIsEmpty(CorporateProfileTable)) {
                boolean insert = m_FileRecord_Db.insertData(CorporateProfileTable, valuesToInsert);
                if (!insert) {
                    result = false;
                    UIController.getInstance().alert("DatabaseController", "CorporateProfileActivity \nInsert failed", R.drawable.ic_error);
                } else {
                    result = true;
                }
            } else {
                boolean update = m_FileRecord_Db.updateData(CorporateProfileTable, CorporateProfile.CORPORATE_NAME, existing_name, valuesToInsert);
                if (!update) {
                    result = false;
                    UIController.getInstance().alert("DatabaseController", "CorporateProfileActivity \nupdate failed", R.drawable.ic_error);
                } else {
                    result = true;
                }
            }
        }
        return result;
    }

    public void insertIntoFileRecordTable(DataObject[] dataObjects) {

        for (int i = 0, t = dataObjects.length; i != t; ++i) {
            HashMap<String, String> valuesToInsert = new HashMap<String, String>();
            DataObject dataObject = dataObjects[i];
            valuesToInsert.put(DataObject.tag_Filename, dataObject.getFilename());
            valuesToInsert.put(DataObject.tag_FileType, dataObject.getFileExtension());
            valuesToInsert.put(DataObject.tag_FileStatus, dataObject.getFileStatus());
            valuesToInsert.put(DataObject.tag_UserLocation, dataObject.getEncryptedLocation());
            valuesToInsert.put(DataObject.tag_FilePassword, dataObject.getFilePassword());

            m_FileRecord_Db.insertData(FileRecordTable, valuesToInsert);
        }
    }

    public ArrayList<DataObject> getFileRecords() {
        ArrayList<DataObject> dataObjects = new ArrayList<DataObject>();
        Cursor cursor = m_FileRecord_Db.getAllData(FileRecordTable);
        int total = cursor.getCount();
        if (total != 0) {
            int index = 0;

            while (cursor.moveToNext()) {
                String filename = cursor.getString(DataObject.index_Filename);
                if (!FileSystem.getInstance().fileExists(filename)) continue;

                String filetype = cursor.getString(DataObject.index_FileType);
                String filestatus = cursor.getString(DataObject.index_FileStatus);
                String userlocation = cursor.getString(DataObject.index_UserLocation);
                String password = cursor.getString(DataObject.index_FilePassword) + "";
                dataObjects.add(new DataObject(filename, filetype, filestatus, userlocation, password));
            }
        }

        return dataObjects;
    }

    public void updateFileRecord(DataObject dataObject) {

        if (instance == null) {
            UIController.getInstance().alert("DatabaseController", "instance is null", R.drawable.ic_error);
            return;
        }
        HashMap<String, String> valuesToInsert = new HashMap<String, String>();
        valuesToInsert.put(DataObject.tag_Filename, dataObject.getFilename());
        valuesToInsert.put(DataObject.tag_FileType, dataObject.getFileExtension());
        valuesToInsert.put(DataObject.tag_FileStatus, dataObject.getFileStatus());
        valuesToInsert.put(DataObject.tag_UserLocation, dataObject.getEncryptedLocation());
        valuesToInsert.put(DataObject.tag_FilePassword, dataObject.getFilePassword());

        boolean update = m_FileRecord_Db.updateData(FileRecordTable, DataObject.tag_Filename, dataObject.getFilename(), valuesToInsert);
        if (!update) {
            UIController.getInstance().alert("DatabaseController", "FileRecord: \nupdate failed", R.drawable.ic_error);
        }
    }


    public void deleteFileRecord(DataObject dataObject) {
        if (instance == null) {
            UIController.getInstance().alert("DatabaseController", "instance is null", R.drawable.ic_error);
            return;
        }
        int delete = m_FileRecord_Db.deleteData(FileRecordTable, dataObject.tag_Filename, dataObject.getFilename());
        //
    }

    public void deleteCorporateProfileTable() {
        if (instance == null) {
            UIController.getInstance().alert("DatabaseController", "instance is null", R.drawable.ic_error);
            return;
        }

        m_FileRecord_Db.deleteTable(CorporateProfileTable);
    }

    public boolean deleteCorporateProfile(String existing_name) {
        if (instance == null) {
            UIController.getInstance().alert("DatabaseController", "instance is null", R.drawable.ic_error);
            return false;
        }
        m_FileRecord_Db.deleteData(CorporateProfileTable, CorporateProfile.CORPORATE_NAME, existing_name);
        deleteCorporateProfileTable ();
        return true;
    }


    public boolean validatePassword (DataObject dataObject) {
        if (instance == null) {
            UIController.getInstance().alert ("DatabaseController", "instance is null", R.drawable.ic_error);
            return false;
        }
        HashMap <String, String> search_values = new HashMap <String, String> ();
        search_values.put (DataObject.tag_Filename, dataObject.getFilename());
        //search_values.put (DataObject.tag_FilePassword, dataObject.getFilePassword());
        Cursor cursor = m_FileRecord_Db.getData (FileRecordTable, search_values);
        int total = cursor.getCount();
        if (total > 0) {
            while (cursor.moveToNext()) {
                String password = cursor.getString (DataObject.index_FilePassword);
                password = (password == null ? "" : password); // to convert null strings to empty strings
                if (dataObject.getFilePassword().equals (password)) {
                    return true;
                } else {
                    UIController.getInstance().alert ("Error", "Incorrect password!", R.drawable.ic_error);
                }
            }
        }
        return false;
    }

    public boolean CorporateProfileExists () {
        return TableExists (CorporateProfileTable) && !TableIsEmpty (CorporateProfileTable);
    }

    public boolean TableExists (String tblName) {
        if (instance == null) {
            UIController.getInstance().alert ("DatabaseController", "instance is null", R.drawable.ic_error);
            return false;
        }
        return m_FileRecord_Db.TableExists(tblName);
    }

    public boolean TableIsEmpty (String tblName) {
        if (instance == null) {
            UIController.getInstance().alert ("DatabaseController", "instance is null", R.drawable.ic_error);
            return true;
        }

        Cursor cursor = m_FileRecord_Db.getAllData (CorporateProfileTable);
        if (cursor != null) {
            int count = cursor.getCount();
            if (count > 0)
                return false;
        }

        return true;
    }
}
