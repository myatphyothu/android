package com.amplestudio.datacrypt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by myatphyothu on 22/9/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // this can be a class structure
    public static final String m_DatabaseName = "datacrypt.db";

    private HashMap<String, ArrayList<DataAttribute>> m_TableStructures = new HashMap<String, ArrayList<DataAttribute>> ();
    //private String m_TableName = "";
    private ArrayList <DataAttribute> m_Columns = new ArrayList <DataAttribute> ();



    public DatabaseHelper (Context context) {
        super (context, m_DatabaseName, null, 1);
    }

    public void addTable (String _tblName, ArrayList <DataAttribute> _dataColumns) {
        if (m_TableStructures.containsKey(_tblName))
            return;
        m_TableStructures.put (_tblName, _dataColumns);

    }


    private String getCreateTableStatement (String tblName) {
        if (!m_TableStructures.containsKey(tblName)) return null;

        String statement = "Create table if not exists " + tblName + " (";
        for (int i = 0, t = m_TableStructures.get(tblName).size(); i != t; ++i) {
            DataAttribute dataAttribute = m_TableStructures.get(tblName).get(i);
            statement += dataAttribute.m_ColumnName + " " + dataAttribute.m_ColumnType;
            statement += (i == 0 ? " PRIMARY KEY AUTOINCREMENT" : "");
            statement += (i != t - 1 ? "," : "");
        }
        statement += ")";
        return statement;
    }

    private String getUpdateTableStatement (String tblName, HashMap <String, String> values, String condition_columnName, String condition_columnValue) {
        /*
            UPDATE table_name
            SET column1 = value1, column2 = value2, ...
            WHERE condition;
         */
        if (!m_TableStructures.containsKey(tblName)) return null;
        String statement = "UPDATE " + tblName + " SET ";
        for (int i = 0, t = m_TableStructures.get(tblName).size(); i != t; ++i) {
            String columnName = m_TableStructures.get(tblName).get(i).m_ColumnName;
            statement +=  columnName + "='" + values.get(columnName) + "'" + (i != t-1 ? "," : "");
        }
        statement += " WHERE " + condition_columnName + " = '" + condition_columnValue + "'";
        return statement;
    }

    private String getDeleteTableStatement (String tblName) {
        if (!m_TableStructures.containsKey(tblName)) return null;
        String statement = "DROP TABLE IF EXISTS " + tblName;

        return statement;
    }

    private String getSelectTableStatement (String tblName) {
        if (!m_TableStructures.containsKey(tblName)) return null;
        String statement = "select ";
        for (int i = 0, t = m_TableStructures.get(tblName).size(); i != t; ++i) {
            statement += m_TableStructures.get(tblName).get(i).m_ColumnName + (i != t-1 ? "," : "");
        }
        statement += " from " + tblName;
        return statement;
    }

    private String getSelectTableStatement (String tblName, HashMap <String, String> values) {

        String statement = getSelectTableStatement(tblName);
        if (statement == null) return null;
        int index = 0;
        int total = values.size();

        if (total > 0)
            statement += " where ";

        for (String key : values.keySet()) {
            statement += key + " = '" + values.get(key) + "'" + (index!=total-1 ? " AND " : "");
        }
        return statement;
    }


    private ContentValues getContentValues (String tblName, HashMap <String, String> values) {
        if (!m_TableStructures.containsKey(tblName)) return null;
        ContentValues contentValues = new ContentValues();
        ArrayList<String> keys = new ArrayList<String> (values.keySet());
        for (int i = 0, t = keys.size(); i != t; ++i) {
            String key = keys.get(i);
            if (containsColumn(tblName, key)) {
                contentValues.put (key, values.get(key));
            } else {
                Log.d ("debug", "Column [" + key + "] does not exist...");
            }
        }
        return contentValues;
    }

    private boolean containsColumn (String tblName, String columnName) {
        if (!m_TableStructures.containsKey(tblName)) return false;
        boolean result = false;
        for (int i = 0, t = m_TableStructures.get(tblName).size(); i != t; ++i) {
            if (m_TableStructures.get(tblName).get(i).m_ColumnName.equals(columnName)) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*String statement = getCreateTableStatement ();
        Log.d ("Debug", statement);
        sqLiteDatabase.execSQL (statement);
        */
        String createCorporateProfile = getCreateTableStatement("corporateprofile");
        sqLiteDatabase.execSQL (createCorporateProfile);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        for (String key : m_TableStructures.keySet()) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ key);
        }

        onCreate(sqLiteDatabase);
    }

    public boolean createTable (String tblName) {
        if (!m_TableStructures.containsKey(tblName)) return false;
        String statement = getCreateTableStatement(tblName);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL (statement);
        return true;
    }

    public boolean insertData(String tblName, HashMap <String, String> values) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = getContentValues(tblName, values);

        long result = db.insert(tblName,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData(String tblName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String statement = getSelectTableStatement(tblName);
        Cursor res = db.rawQuery (statement, null);
        return res;
    }

    public Cursor getData (String tblName, HashMap <String, String> searchValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        String statement = getSelectTableStatement (tblName,searchValues);
        Cursor res = db.rawQuery (statement, null);
        return res;
    }

    public boolean updateData(String tblName, String search_column, String search_value, HashMap <String, String> values) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = getContentValues(tblName, values);
        //String statement = getUpdateTableStatement (tblName, values, search_column, search_value);
        //db.execSQL(statement);

        db.update(tblName, contentValues, search_column + " = ?", new String[] { search_value });

        return true;
    }

    public int deleteData (String tblName, String search_column, String search_value) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(tblName, search_column + " = ?",new String[] {search_value});

    }

    public boolean deleteTable (String tblName) {
        if (!m_TableStructures.containsKey(tblName)) return false;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(getDeleteTableStatement(tblName));
        return true;
    }

    public boolean TableExists (String tblName) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"
                + tblName + "'", null);

        return cursor.getCount() > 0;
    }
}
