package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 24/8/17.
 */

public enum FILE_TYPE {
    FILE_TYPE_TEXT,
    FILE_TYPE_PDF,
    FILE_TYPE_IMAGE,
    FILE_TYPE_AUDIO,
    FILE_TYPE_VID,
    FILE_TYPE_BINARY,
    FILE_TYPE_DOCX,
    FILE_TYPE_ERROR
}
