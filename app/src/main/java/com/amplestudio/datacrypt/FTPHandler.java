package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 30/10/17.
 */

import android.util.Log;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by myatphyothu on 12/10/17.
 */

public class FTPHandler {
    private FTPClient mFTPClient = null;
    private static final String TAG = "FTP Handler";
    private static FTPHandler instance = null;
    private final int mConnectionTimeout = 10000; // 10 seconds

    private FTPHandler() {}

    public static FTPHandler getInstance () {
        if (instance == null)
            instance = new FTPHandler();

        return instance;
    }


    public boolean ftpConnect(String host, String username,
                              String password, int port) {
        try {
            mFTPClient = new FTPClient();
            mFTPClient.setConnectTimeout(mConnectionTimeout);
            // connecting to the host
            //InetAddress address = InetAddress.getByName (host);
            mFTPClient.connect(host, port);
            boolean isPositiveCompletion = FTPReply.isPositiveCompletion(mFTPClient.getReplyCode());
            mFTPClient.enterLocalPassiveMode();
            //mFTPClient.connect(InetAddress.getByName(host), port);

            // now check the reply code, if positive mean connection success
            if (isPositiveCompletion) {
                // login using username & password
                boolean status = mFTPClient.login(username, password);


            /* Set File Transfer Mode
             *
             * To avoid corruption issue you must specified a correct
             * transfer mode, such as ASCII_FILE_TYPE, BINARY_FILE_TYPE,
             * EBCDIC_FILE_TYPE .etc. Here, I use BINARY_FILE_TYPE
             * for transferring text, image, and compressed files.
             */
                mFTPClient.setFileType(FTPClient.ASCII_FILE_TYPE | FTPClient.BINARY_FILE_TYPE);

                //mFTPClient.enterRemoteActiveMode(address, port);
                //mFTPClient.enterLocalPassiveMode();

                return status;
            }
        } catch(Exception e) {
            Log.d(TAG, "Error: could not connect to host " + host );
            Log.d(TAG, e.toString());
        }

        return false;
    }

    public boolean isConnected () {
        if (mFTPClient == null) return false;
        return mFTPClient.isConnected();
    }

    public boolean ftpDisconnect() {
        try {
            mFTPClient.logout();
            mFTPClient.disconnect();
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Error occurred while disconnecting from ftp server.");
        }

        return false;
    }

    public String ftpGetCurrentWorkingDirectory() {
        try {
            String workingDir = mFTPClient.printWorkingDirectory();
            return workingDir;
        } catch(Exception e) {
            Log.d(TAG, "Error: could not get current working directory.");
        }

        return null;
    }

    public boolean ftpChangeDirectory(String directory_path)
    {
        try {
            mFTPClient.changeWorkingDirectory(directory_path);
        } catch(Exception e) {
            Log.d(TAG, "Error: could not change directory to " + directory_path);
        }

        return false;
    }

    public FTPFile[] ftpPrintFilesList(String dir_path) {
        try {
            FTPFile[] ftpFiles = mFTPClient.listFiles(dir_path);
            /*String names[] = mFTPClient.listNames();
            if (names != null) {
                int length = names.length;
            }*/

            int length = ftpFiles.length;
            return ftpFiles;
            /*
            int length = ftpFiles.length;

            for (int i = 0; i < length; i++) {
                String name = ftpFiles[i].getName();
                boolean isFile = ftpFiles[i].isFile();

                if (isFile) {
                    Log.i(TAG, "File : " + name);
                }
                else {
                    Log.i(TAG, "Directory : " + name);
                }
            }*/
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean ftpMakeDirectory(String new_dir_path) {
        try {
            boolean status = mFTPClient.makeDirectory(new_dir_path);
            return status;
        } catch(Exception e) {
            Log.d(TAG, "Error: could not create new directory named " + new_dir_path);
        }

        return false;
    }

    public boolean ftpRemoveDirectory(String dir_path) {
        try {
            boolean status = mFTPClient.removeDirectory(dir_path);
            return status;
        } catch(Exception e) {
            Log.d(TAG, "Error: could not remove directory named " + dir_path);
        }

        return false;
    }

    public boolean ftpRemoveFile(String filePath) {
        try {
            boolean status = mFTPClient.deleteFile(filePath);
            return status;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean ftpRenameFile(String from, String to) {
        try {
            boolean status = mFTPClient.rename(from, to);
            return status;
        } catch (Exception e) {
            Log.d(TAG, "Could not rename file: " + from + " to: " + to);
        }

        return false;
    }

    /**
     * mFTPClient: FTP client connection object (see FTP connection example)
     * srcFilePath: path to the source file in FTP server
     * desFilePath: path to the destination file to be saved in sdcard
     */
    public boolean ftpDownload(String srcFilePath, String desFilePath) {
        boolean status = false;
        try {
            FileOutputStream desFileStream = new FileOutputStream(desFilePath);;
            status = mFTPClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();

            return status;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "download failed");
        }

        return status;
    }

    /**
     * mFTPClient: FTP client connection object (see FTP connection example)
     * srcFilePath: source file path in sdcard
     * desFileName: file name to be stored in FTP server
     * desDirectory: directory path where the file should be upload to
     */
    public boolean ftpUpload(String srcFilePath, String desFileName,
                             String desDirectory) {
        boolean status = false;
        try {
            FileInputStream srcFileStream = new FileInputStream(srcFilePath);

            // change working directory to the destination directory
            if (ftpChangeDirectory(desDirectory)) {
                status = mFTPClient.storeFile(desFileName, srcFileStream);
            }

            srcFileStream.close();
            return status;
        } catch (Exception e) {
            Log.d(TAG, "upload failed");
        }

        return status;
    }

    public FTPFile[] getFTPFiles (String ftpDirectory, String localDirectory) {
        if (!isConnected()) return null;
        FTPFile[] ftpFiles;

        ftpFiles = ftpPrintFilesList(ftpDirectory);

        return ftpFiles;
    }

    public boolean downloadDataFiles (String ftpDirectory, String localDirectory) {
        if (!isConnected()) return false;
        FTPFile[] ftpFiles;

        String localDataDirectory = ApplicationContext.get().getApplicationInfo().dataDir;
        String workingDir = ftpGetCurrentWorkingDirectory();
        if (workingDir == null) return false;
        String currentDir = workingDir + ftpDirectory;

        ftpFiles = ftpPrintFilesList(currentDir);
        int totalFiles = 0;
        int totalDownloadedFiles = 0;
        boolean download = false;

        FileSystem.getInstance().CreateDirectory(localDirectory);

        for (int i = 0, t = ftpFiles.length; i != t; ++i) {
            FTPFile  file = ftpFiles[i];
            if (file.isFile()) {
                ++totalFiles;
                String downloadPath = localDataDirectory + "/" + localDirectory;
                File localFile = new File (downloadPath + "/" + file.getName());
                if (localFile.exists()) {
                    localFile.delete();
                }
                //UIController.getInstance().alert ("FTP Download...", "Downloading " + file.getName() + "...", R.drawable.ic_information);
                //UIController.getInstance().displayStatus("Downloading " + file.getName() + "...", UIController.mColorDarkGray);
                download = ftpDownload(currentDir + "/" + file.getName(), downloadPath + "/" + file.getName());
                if (download) ++totalDownloadedFiles;

            }
        }

        if (totalDownloadedFiles != totalFiles) {
            //UIController.getInstance().displayStatus("Only " + totalDownloadedFiles + "/" + totalFiles + " were downloaded...", UIController.mColorRed);
            return false;
        } else{
            return true;
        }

    }

    public boolean forceDownloadFile (String ftpDirectory, String localDirectory, String filename) {
        if (!isConnected()) return false;

        FTPFile[] ftpFiles;

        String dataDir = ApplicationContext.get().getApplicationInfo().dataDir;
        String workingDir = ftpGetCurrentWorkingDirectory();
        if (workingDir == null) return false;

        String currentDir = workingDir + ftpDirectory;
        ftpFiles = ftpPrintFilesList(currentDir);
        boolean download = false;

        if (filename == null) return false;

        if (ftpFiles == null) {
            UIController.getInstance().displayStatus("No files under " + currentDir + "...", UIController.mColorRed);
            return false;
        }
        FileSystem.getInstance().CreateDirectory(localDirectory);

        for (int i = 0, t = ftpFiles.length; i != t; ++i) {
            FTPFile  file = ftpFiles[i];
            if (file.isFile()) {
                String ftpFilename = file.getName();
                if (ftpFilename.equals(filename)) {

                    download = ftpDownload(currentDir + "/" + file.getName(), dataDir + "/" + localDirectory + "/" + file.getName());
                    if (!download) {
                        UIController.getInstance().displayStatus("Unable to download " + filename + "...", UIController.mColorRed);
                        return false;
                    }
                    break;
                }
            }
        }
        return true;
    }

}