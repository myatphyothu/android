package com.amplestudio.datacrypt;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by myatphyothu on 23/9/17.
 */

public class FileSystem {

    private static FileSystem instance = null;
    private File m_File = null;
    DataObject[] m_DataObjects  = null;
    public static boolean mDownloadDataFiles = false;
    public static final String mStorageDir = "Decrypted";

    private FileSystem() {
        m_File = ApplicationContext.get().getFilesDir();
    }

    public static FileSystem getInstance () {
        if (instance == null)  {
            instance = new FileSystem ();
        }
        return instance;
    }

    /*
    public void CreateDirectory () {
        String directoryName = ApplicationContext.get().getFilesDir().getName();
        String path = ApplicationContext.get().getFilesDir().getAbsolutePath();
        //Log.d ("CREATION", "Internal Storage: " + directoryName);
        //File file = new File (m_Context.getFilesDir(), m_directoryName);
    }
    */

    public boolean fileExists (String filename) {
        File file = new File(filename);
        if (file.exists())
            return true;

        return false;
    }

    public void createDummyFiles () {
        String[] filenames = new String[] { "text_file.txt", "video_file.mp4", "image_file.png", "music_file.mp3", "pdf_file.pdf", "image_file.jpg" };
        String[] contents = new String [] {
                "Text file data contents here,",
                "Video file contents",
                "Image file contents",
                "Music file contents",
                "PDF file contents",
                "Image file contents"
        };

        int total = filenames.length;
        for (int i = 0; i != total; ++i) {
            CreateFile (filenames[i], contents[i]);
        }
        createDataObjects(filenames);
    }

    public void CreateDirectory (String name) {
        String dir = ApplicationContext.get().getApplicationInfo().dataDir + "/" + name;
        File file = new File (dir);
        if (!file.exists()) {
            file.mkdir();
        }

    }

    public void CreateFile (String filename, String contents) {

        if (m_File == null) return;
        File file = new File (m_File, filename);
        FileOutputStream fileOutputStream;

        if (file.exists()) {
            // actions
            return;
        }

        try {
            fileOutputStream = ApplicationContext.get().openFileOutput(filename, Context.MODE_PRIVATE);
            fileOutputStream.write (contents.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int dataFileCount (String dir) {
        int total = 0;
        File file = new File (dir);
        if (file.exists()) {
            File[] files = file.listFiles();
            for (File subfile : files) {
                if (subfile.isFile()) ++ total;
            }
        }
        return total;
    }

    public void DeleteFile (String filename) {
        File file = new File (filename);
        if (!file.exists()) {
            return;
        }

        if (!file.isFile()) return;

        boolean delete = file.delete();
        if (!delete) {
            UIController.getInstance().alert ("Error!", "File deletion failed...", R.drawable.ic_error);
        }else {
            UIController.getInstance().alert ("Success!", "File deleted...", R.drawable.ic_success);
        }
    }

    private void createDataObjects(String[] filenames) {

        m_DataObjects = new DataObject [filenames.length];

        for (int i = 0, t = m_DataObjects.length; i != t; ++i) {
            // String _filename, String _filetype, String _filestatus, String _location, String _password

            String filename = filenames[i];
            m_DataObjects[i] = new DataObject (filename, filename.substring (filename.lastIndexOf('.')+1), "false", "", "");
        }

        DatabaseController.getInstance().insertIntoFileRecordTable (m_DataObjects);
    }

    public DataObject[] GetDataObjects () {
        DataObject[] dataObjects = null;

        return m_DataObjects;
    }

    public String getFileContents (String filename) {
        String data = "";
        File file = new File (filename);

        if (!file.exists()) {
            UIController.getInstance().displayStatus(filename + " does not exist...", UIController.mColorRed);
            return null;
        }

        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            int content;
            while ((content = fileInputStream.read()) != -1) {
                data += (char) content;
            }
            data = data.substring(0, data.length()-1);
            fileInputStream.close();
            //UIController.getInstance().displayStatus(finalContent + "", UIController.mColorGreen);
        } catch (Exception e) {
            e.printStackTrace();
            UIController.getInstance().displayStatus("Error in reading " + filename, UIController.mColorRed);
            return null;
        }
        return data;
    }

    public boolean locationFileExists () {
        return fileExists(ApplicationContext.get().getApplicationInfo().dataDir + "/Corporate/location.txt");
    }

    public void retrieveCorporateLocationData() {
        String filename = "location.txt";
        String dataDirectory = ApplicationContext.get().getApplicationInfo().dataDir;
        CorporateProfile mCorporateProfile = CorporateProfile.getInstance();

        // read file
        String fileContents = getFileContents(dataDirectory + "/Corporate/" + filename);

        // validate file contents
        if (fileContents != null) {
            String[] dataArray = fileContents.split(",");
            if (dataArray.length < 3) {
                UIController.getInstance().displayStatus("Location file content error...", UIController.mColorRed);
            } else {
                mCorporateProfile.mLocation = dataArray[0] + "," + dataArray[1];
                mCorporateProfile.mVicinity = dataArray[2];

                if (!mCorporateProfile.locationDataUpdated) {
                    DatabaseController.getInstance().updateCorporateProfileTable();
                    mCorporateProfile.locationDataUpdated = true;
                }

                UIController.getInstance().updateCorporateLocation(mCorporateProfile.mLocation);
                UIController.getInstance().updateCorporateVicinity(mCorporateProfile.mVicinity + " km");
            }
        }
    }

    public void retrieveDataFiles () {
        String dirName = ApplicationContext.get().getApplicationInfo().dataDir;
        File dir = new File (dirName + "/Data");
        if (!dir.exists()) return;
        File[] subFiles = dir.listFiles();
        ArrayList<DataObject> dataObjects = new ArrayList <DataObject> ();
        if (subFiles != null) {
            for (File file : subFiles) {
                String filename = file.getName();
                if (file.isFile()) {
                    DataObject dataObject = new DataObject(file);
                    dataObjects.add(dataObject);
                }
            }
            UIController.getInstance().addDataObjects(dataObjects);
        }
    }

    public byte[] decryptFile (String directory, String filename, String password) {
        File dir = new File (directory);
        if (!dir.exists()) {
            return null;
        }
        File dataFile = new File (dir + "/" + filename);
        if (!dataFile.exists())
            return null;

        FileInputStream fin = null;
        byte[] decryptedContent = null;
        try {
            String contents = "";
            fin = new FileInputStream (dataFile);
            byte fileContent[] = new byte[(int)dataFile.length()];
            fin.read (fileContent);
            contents = new String (fileContent);

            //print (contents);
            fin.close ();
            AES.getInstace().setKeyPhrase(password);
            try {
                decryptedContent = AES.getInstace().decrypt(fileContent);
                return decryptedContent;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        } catch (IOException ex) {
            ex.printStackTrace ();
            return null;
        }
    }

    public String writeDecryptedContentsToFile(String filename, byte[] decryptedContents) {
        //File file = new File(ApplicationContext.get().getExternalFilesDir(null), filename);
        String directory = ApplicationContext.get().getApplicationInfo().dataDir + "/" + mStorageDir;
        String fileFullPath = directory + "/" + filename;
        CreateDirectory(directory);
        File file = new File (fileFullPath);

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file, false);
            fileOutputStream.write(decryptedContents);
            fileOutputStream.close();

            return fileFullPath;
        } catch (IOException e) {
            e.printStackTrace();
            UIController.getInstance().alert("Error", "Unable to write decrypted contents of " + filename + " to the system.", R.drawable.ic_error);
            return null;
        }
    }

    public String writeDecryptedContentsToStorage (String filename, byte[] decryptedContents) {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(path, filename);
        String fullFilePath = file.getAbsolutePath();
        boolean b1 = path.exists();
        try {
            path.mkdirs();
            if (path.exists()) {
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file, false);
            fileOutputStream.write (decryptedContents);
            fileOutputStream.close ();
        } catch (IOException e) {
            e.printStackTrace();
            UIController.getInstance().alert("Error", "Unable to write decrypted contents of " + filename + " to the system.", R.drawable.ic_error);
            return null;
        }
        return fullFilePath;
    }

    public void openFileWithApp (String filename, String appType) {
        File file = new File (filename);

        if (!file.exists()) {
            UIController.getInstance().alert ("Error", "The file does not exist...", R.drawable.ic_error);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);

        //Uri uri = Uri.fromFile(file);
        String authority = ApplicationContext.get().getApplicationContext().getPackageName() + ".provider";
        Uri uri = FileProvider.getUriForFile (ApplicationContext.get(), authority, file);

        //Uri fileURI = Uri.parse("content://" + file.getAbsolutePath());

        //intent.setDataAndType (fileURI, appType);
        //intent.putExtra (Intent.EXTRA_STREAM, fileURI);

        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setDataAndType(uri, appType);

        PackageManager pm = ApplicationContext.get().getPackageManager();
        if (intent.resolveActivity(pm) != null)
            UIController.getInstance().startActivity(intent);
    }


    public boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


}
