package com.amplestudio.datacrypt;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by myatphyothu on 30/9/17.
 */

public class LocationService {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private static LocationService instance = null;
    private LocationManager m_LocationManager = null;
    private LocationListener m_LocationListener = null;
    private Context m_Context = null;
    private MainActivity m_MainActivity = null;
    private static String m_UserLocation = "";

    private static int m_DecryptionRadius = 3; // in km
    private final String m_DefaultLocation = "1.3,103.856";
    public final static int m_DecryptionRadiusMin = 1; // in km
    public final static int m_DecryptionRadiusMax = 10; // in km
    public static boolean mIsWithinVicinity = false;
    public static float mDistance = 0.0f;

    private Boolean mRequestingLocationUpdates = true;
    private String mLastUpdateTime;
    private String mLatitude;
    private String mLongitude;

    public static LocationService getInstance() {
        if (instance == null)
            instance = new LocationService();
        return instance;
    }

    private LocationService() {

    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
            //updateUI();
            mLatitude = mCurrentLocation.getLatitude() + "";
            mLongitude = mCurrentLocation.getLongitude() + "";
        }
    }

    void init(MainActivity _mainActivity) {
        if (m_MainActivity == null) {
            m_MainActivity = _mainActivity;
            m_Context = m_MainActivity.getApplicationContext();
            mRequestingLocationUpdates = true;
            mLastUpdateTime = "";
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(m_MainActivity);
            mSettingsClient = LocationServices.getSettingsClient(m_MainActivity);

            createLocationCallback();
            createLocationRequest();
            buildLocationSettingsRequest();
        }
    }

    public boolean GetRequestingLocationUpdates () {
        return true;
        //return mRequestingLocationUpdates;
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                //updateLocationUI();
                UIController.getInstance().updateUserLocation();

                computeDistance ();

            }
        };
    }


    public void computeDistance () {
        if (CorporateProfile.getInstance().mLocation.equals("0,0")) {
            UIController.getInstance().displayLocationStatus("Download corporate location file first from the menu...", UIController.mColorRed);
            return;
        }
        mDistance = getDistanceBetween ();
        if (mDistance >= 0) {
            if (!CorporateProfile.getInstance().isLoggedOn)
                CorporateProfile.getInstance().promptLogin = true;

            if (mDistance < (float)get_DecryptionRadius()) { // within coporate parameters
                mIsWithinVicinity = true;
                UIController.getInstance().displayLocationStatus("You're within corporate vicinity...", UIController.mColorGreen);
            } else {
                mIsWithinVicinity = false;
                UIController.getInstance().displayLocationStatus("You're " + mDistance + " km away from corporate...", UIController.mColorRed);
            }
        } else {
            CorporateProfile.getInstance().isLoggedOn = false;
            UIController.getInstance().displayUserStatus(CorporateProfile.msg_LoggedOff, UIController.mColorRed);
            UIController.getInstance().displayLocationStatus("Error in distance calculation", UIController.mColorRed);
        }
    }

    private float  getDistanceBetween () {
        float distance = -1;
        CorporateProfile corporateProfile = null;
        if (CorporateProfile.getInstance().exists) {
            corporateProfile = CorporateProfile.getInstance();
            distance = distanceBetween (getUserLocation(), corporateProfile.mLocation);
        }

        return distance;
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    public void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(m_MainActivity, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        //updateUI();
                    }
                })
                .addOnFailureListener(m_MainActivity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(m_MainActivity, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;

                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                //Toast.makeText(, errorMessage, Toast.LENGTH_LONG).show();
                                UIController.getInstance().alert ("Location Services" , errorMessage, R.drawable.ic_error);
                                mRequestingLocationUpdates = false;
                                break;
                        }

                        //updateUI();
                    }
                });
    }

    public void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(m_MainActivity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }


    /**
     * Return the current state of the permissions needed.
     */
    public boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(m_MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(m_MainActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            ActivityCompat.requestPermissions(m_MainActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(m_MainActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public void configureButton() {

        //m_LocationManager.requestLocationUpdates("gps", 5000, 0, m_LocationListener);
    }

    public void setUserLocation (String value) { m_UserLocation = value; }
    public String getUserLocation () {
        String result = "";
        if (mCurrentLocation != null) {
            String latVal = String.format("%.2f", mCurrentLocation.getLatitude());
            String lngVal = String.format("%.2f", mCurrentLocation.getLongitude());
            result = latVal + "," + lngVal;
        } else {
            result = m_DefaultLocation;
        }
        return result;
    }
    public void setDecryptionRadius (int value) { m_DecryptionRadius = value; }

    public int get_DecryptionRadius () { return m_DecryptionRadius; }

    public float[] getDistance (LatLng p1, LatLng p2) {
        float[] results = new float[3];
        Location.distanceBetween(p1.latitude, p1.longitude, p2.latitude, p2.longitude, results);
        return results;
    }

    /*
    public String distanceToString (DataObject dataObject) {
        String[] userLatLngString = m_UserLocation.split (" ");
        String[] dataLatLngString = dataObject.getEncryptedLocation().split (" ");
        String result = "";

        LatLng userLatLng = new LatLng (Double.parseDouble (userLatLngString[0]), Double.parseDouble (userLatLngString[1]));
        LatLng dataLatLng = new LatLng (Double.parseDouble (dataLatLngString[0]), Double.parseDouble (dataLatLngString[1]));

        float[] distanceInfo = getDistance (userLatLng, dataLatLng);

        if (distanceInfo.length > 0)
            result = Math.round((distanceInfo [0]/1000) * 100.0) / 100.0 + ""; // divide by 1000 to convert to km ( *100.0/100.0 to round to 2 decimal places)
        else
            result = "error";

        return result;
    }
    */

    public float distanceBetween (String location0, String location1) {
        float result = 0;
        String[] LatLngString0 = location0.split (",");
        String[] LatLngString1 = location1.split (",");
        LatLng latlng0 = new LatLng (Double.parseDouble (LatLngString0[0]), Double.parseDouble (LatLngString0[1]));
        LatLng latlng1 = new LatLng (Double.parseDouble (LatLngString1[0]), Double.parseDouble (LatLngString1[1]));
        float[] distanceInfo = getDistance (latlng0, latlng1);
        if (distanceInfo.length > 0)
            result = (float) Math.round((distanceInfo [0]/1000) * 100.0f) / 100.0f; // divide by 1000 to convert to km ( *100.0/100.0 to round to 2 decimal places)
        else
            result = -1f;
        return result;
    }

    public String[] getDummyLocations () {
        // 1.322852, 103.856018
        // 1.313670, 103.862370
        // 1.307263, 103.835482
        // 1.295900, 103.848760
        // 1.285051, 103.863204
        // 1.291965, 103.873788
        LatLng[] latlngArray = new LatLng[] {
                new LatLng (1.322852, 103.856018),
                new LatLng (1.313670, 103.862370),
                new LatLng (1.307263, 103.835482),
                new LatLng (1.295900, 103.848760),
                new LatLng (1.285051, 103.863204),
                new LatLng (1.291965, 103.873788)
        };
        String [] results = new String [latlngArray.length];
        for (int i = 0, t = results.length; i != t; ++i)
            results[i] = latlngArray[i].latitude + "," + latlngArray[i].longitude;

        return results;
    }

    public String getDummyLocation () {
        String[] locations = getDummyLocations();
        int min = 0;
        int max = locations.length - 1;
        Random random  = new Random ();
        int genNumber = random.nextInt (max-min+1) + min;
        return locations [genNumber];
    }


}
