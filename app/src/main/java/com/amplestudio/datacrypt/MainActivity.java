package com.amplestudio.datacrypt;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Context m_Context;
    private Handler mHandler;
    private CorporateProfile mCorporateProfile = null;
    private boolean mCorporateProfileExists = false;
    private FTPHandler mFTPHandler = null;
    private ThreadController mThread = null;
    LinearLayout mLinearLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApplicationContext.getInstance().init(getApplicationContext());
        //use via ApplicationContext.get()
        assert (getApplicationContext() == ApplicationContext.get());

        mThread = ThreadController.getInstance();
        InitUIController ();
        InitDatabase ();

        InitLocationServices();

        mCorporateProfileExists = DatabaseController.getInstance().CorporateProfileExists();

        // check corporate profile exists
        // if it doesn't, start CorporateProfileActivity activity


        if (!mCorporateProfileExists) {
            UIController.getInstance().startCorporateProfileActivity (mCorporateProfileExists);

        } else {

            UIController.getInstance().initLocationTextFields();

            DatabaseController.getInstance().retrieveCorporateProfile();
            mCorporateProfile = CorporateProfile.getInstance();
            LocationService.getInstance().computeDistance();

            mThread.initThread_FTPConnection();
            mThread.initThread_CorporateLocationFileDownload ();
            mThread.initThread_DataFileDownload();

            if (FileSystem.getInstance().locationFileExists()) {
                FileSystem.getInstance().retrieveCorporateLocationData();
            } else {
                mCorporateProfile.downloadCorporateLocation = true;
            }

            //if (mCorporateProfile.isLoggedOn) {
                FileSystem.getInstance().retrieveDataFiles();
                UIController.getInstance().createSwipeListView();
            //}
        }
    }

    @Override
    public void onResume () {

        super.onResume();

        if (LocationService.getInstance().GetRequestingLocationUpdates() && LocationService.getInstance().checkPermissions()) {
            LocationService.getInstance().startLocationUpdates();
        } else if (!LocationService.getInstance().checkPermissions()) {
            LocationService.getInstance().requestPermissions();
        }
        UIController.getInstance().updateUserLocation();
        //updateUI();
    }

    @Override
    public void onPause () {
        super.onPause();
        LocationService.getInstance().stopLocationUpdates();
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
        FTPHandler.getInstance().ftpDisconnect();
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {

        MenuItem corporateMenuItem = menu.findItem(R.id.edit_corporate_profile);
        if (corporateMenuItem != null) {
            mCorporateProfileExists = DatabaseController.getInstance().CorporateProfileExists();
            corporateMenuItem.setTitle (mCorporateProfileExists ? "Edit Corporate Profile" : "Add Corporate Profile");
            return true;
        }

        MenuItem userLoginMenuItem = menu.findItem(R.id.user_login);
        if (userLoginMenuItem != null) {
            userLoginMenuItem.setTitle (CorporateProfile.getInstance().isLoggedOn ? "Sign off" : "Sign on");
            return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*
            case R.id.view_on_map:
                UIController.getInstance().startMapActivity();
                return true;
                */
            case R.id.user_login:
                userLogin();
                return true;

            case R.id.edit_corporate_profile:
                mCorporateProfileExists = DatabaseController.getInstance().CorporateProfileExists();
                UIController.getInstance().startCorporateProfileActivity(mCorporateProfileExists);
                return true;
            case R.id.download_corporate_location:
                if (!CorporateProfile.getInstance().exists) {
                    UIController.getInstance().alert("Error", "Corporate profile does not exist yet. Please create one first", R.drawable.ic_error);
                    return true;
                }
                if (!FTPHandler.getInstance().isConnected()) {
                    UIController.getInstance().displayStatus("FTP Connection not established...", UIController.mColorRed);
                } else {
                    mCorporateProfile.downloadCorporateLocation = true;
                }

                return true;
            case R.id.download_data_files:
                if (!CorporateProfile.getInstance().exists) {
                    UIController.getInstance().alert("Error", "Corporate profile does not exist yet. Please create one first", R.drawable.ic_error);
                    return true;
                }
                if (CorporateProfile.getInstance().mLocation.equals("0,0")){
                    UIController.getInstance().displayStatus("Download corporate location file first from the menu...", UIController.mColorRed);
                } else {
                    LocationService.getInstance().computeDistance();
                    if (LocationService.getInstance().mIsWithinVicinity)
                        FileSystem.mDownloadDataFiles = true;
                    else
                        UIController.getInstance().alert("Error", "You are not within coporate vicinity", R.drawable.ic_error);
                }
                return true;
            /*case R.id.refresh_list_view:
                UIController.getInstance().refereshSwipeListView();
                return true;*/
        }
        return false;
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(LocationService.TAG, "onRequestPermissionResult");
        if (requestCode == LocationService.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(LocationService.TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (LocationService.getInstance().GetRequestingLocationUpdates()) {
                    Log.i(LocationService.TAG, "Permission granted, updates requested, starting location updates");
                    LocationService.getInstance().startLocationUpdates();
                }
            } else {
                // Permission denied.
                UIController.getInstance().alert ("Error", "Permission denied", R.drawable.ic_error);
            }
        }
    }

    private void InitLocationServices () {
        LocationService.getInstance().init(this);
        UIController.getInstance().updateUserLocation();
    }

    private void InitDatabase () {
        //m_DatabaseController.insertIntoFileRecordTable(m_FileExplorerForDb.GetDataObjects());
        DatabaseController.getInstance().init();
    }

    private void InitUIController () {
        mLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_main);

        UIController.getInstance().init (this);
    }

    @Override
    public void onBackPressed() {
    }

    private void userLogin () {
        mCorporateProfileExists = DatabaseController.getInstance().CorporateProfileExists();
        if (mCorporateProfileExists) {
            mCorporateProfile.isLoggedOn = !mCorporateProfile.isLoggedOn;

            if (mCorporateProfile.isLoggedOn)
                UIController.getInstance().displayUserStatus(mCorporateProfile.msg_LoggedOn, UIController.mColorGreen);
            else
                UIController.getInstance().displayUserStatus(mCorporateProfile.msg_LoggedOff, UIController.mColorRed);
        }
    }

    private void showFiles(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        StringBuffer stringBuffer = new StringBuffer();
        ArrayList<DataObject> dataObjects = DatabaseController.getInstance().getFileRecords();
        for (int i = 0, t = dataObjects.size(); i != t; ++i) {
            DataObject dObj = dataObjects.get(i);
            stringBuffer.append ("filename: " + dObj.getFilename() + "\n");
            stringBuffer.append ("extension: " + dObj.getFileExtension() + "\n");
            stringBuffer.append ("status: " + dObj.getFileStatus() + "\n");
            stringBuffer.append ("userlocation: " + dObj.getEncryptedLocation() + "\n");
            stringBuffer.append ("password: " + dObj.getFilePassword() + "\n\n");
        }
        builder.setCancelable(true);
        builder.setTitle("Files");
        builder.setMessage(stringBuffer.toString());
        builder.show();
    }

    private void showCorporateProfile(){
        CorporateProfile corporateProfile = CorporateProfile.getInstance();
        if (corporateProfile != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append ("Name: " + corporateProfile.mName + "\n");
            stringBuffer.append ("URL: " + corporateProfile.mURL + "\n");
            stringBuffer.append ("Port: " + corporateProfile.mPort + "\n");
            stringBuffer.append ("Username: " + corporateProfile.mUsername + "\n");
            stringBuffer.append ("Password: " + corporateProfile.mPassword + "\n");
            stringBuffer.append ("Location: " + corporateProfile.mLocation + "\n");

            builder.setCancelable(true);
            builder.setTitle("Corporate Profile");
            builder.setMessage(stringBuffer.toString());
            builder.show();
        }
    }
}
