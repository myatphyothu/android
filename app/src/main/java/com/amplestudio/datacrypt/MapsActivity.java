package com.amplestudio.datacrypt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String m_Filename = "null";
    LatLng m_UserLocation = null;
    private double m_UserRadius = 0;
    private Circle m_Circle;

    ArrayList<MarkerOptions> m_MapMarkers = new ArrayList <MarkerOptions> ();

    private void receiveIntent () {
        Intent receivedIntent = getIntent();
        Bundle extras = receivedIntent.getExtras();
        if (extras == null)
            return;

        //m_Filename = extras.getString (DataObject.tag_Filename);
        String userLocation = extras.getString ("My Location");
        String[] latlngString = userLocation.split(" ");
        m_UserLocation = new LatLng (Double.parseDouble(latlngString[0]), Double.parseDouble(latlngString[1]));
        m_UserRadius = extras.getDouble ("Radius") * 1000; // convert to meters
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        receiveIntent();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
        markUserOnMap ();
    }

    private void markUserOnMap () {
        if (m_UserLocation == null) return;
        mMap.addMarker (new MarkerOptions().position(m_UserLocation).title ("I'm here"));
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(m_UserLocation).radius(m_UserRadius).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);

        m_Circle = mMap.addCircle(circleOptions);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(m_UserLocation, 18.0f));
    }
}
