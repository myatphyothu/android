package com.amplestudio.datacrypt;

import android.os.Handler;

import org.apache.commons.net.ftp.FTPFile;

import java.io.File;

/**
 * Created by myatphyothu on 8/11/17.
 */

public class ThreadController {

    private static ThreadController instance = null;
    private Thread mThread_FTPConnection = null;
    private Thread mThread_CorporateFileDownload = null;
    private Thread mThread_DataFileDownload = null;
    private Thread mThread_UserLogin = null;
    public boolean updateListView = false;
    private Handler UIHandler;

    private int mThreadWaitTime_CorporateLocation = 500;
    private int mThreadWaitTime_FileDownload = 500;
    private int mThreadWaitTime_Connection = 1000;

    private ThreadController () {
        UIHandler = new Handler();
    }

    public static ThreadController getInstance() {
        if (instance == null)
            instance = new ThreadController();

        return instance;
    }

    public void initThread_FTPConnection () {
        if (mThread_FTPConnection != null) {
            return;
        }

        mThread_FTPConnection = new Thread(new Runnable() {
            public void run() {
                final FTPHandler mFTPHandler = FTPHandler.getInstance();
                final CorporateProfile mCorporateProfile = CorporateProfile.getInstance();


                while (true) {
                    while (!mFTPHandler.isConnected()) {

                        UIHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                UIController.getInstance().displayConnectionStatus ("Connecting...", UIController.mColorDarkGray);
                            }
                        });

                        String host = mCorporateProfile.mURL;
                        int port = Integer.parseInt (mCorporateProfile.mPort);
                        String user = mCorporateProfile.mUsername;
                        String password =  mCorporateProfile.mPassword;

                        boolean ftpConnected = mFTPHandler.ftpConnect(host, user, password, port);
                        if (!ftpConnected) {
                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    UIController.getInstance().displayConnectionStatus("Error. Some parameters may be wrong", UIController.mColorRed);
                                }
                            });

                            mCorporateProfile.isLoggedOn = true;

                        }else {
                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    UIController.getInstance().displayConnectionStatus("Online", UIController.mColorGreen);
                                }
                            });

                            mCorporateProfile.isLoggedOn = false;
                        }
                    }

                    //UIController.getInstance().displayConnectionStatus ("Online", UIController.mColorGreen);
                    try {
                        Thread.sleep(mThreadWaitTime_Connection);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        mThread_FTPConnection.start();

    }

    public void initThread_CorporateLocationFileDownload () {
        if (mThread_CorporateFileDownload != null) return;

        mThread_CorporateFileDownload = new Thread (new Runnable(){
            @Override
            public void run() {
                final FTPHandler mFTPHandler = FTPHandler.getInstance();
                final CorporateProfile mCorporateProfile = CorporateProfile.getInstance();

                final String filename = "location.txt";
                while (true) {
                    while (mCorporateProfile.downloadCorporateLocation) {
                        if (!mFTPHandler.isConnected()) continue;

                        UIHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                UIController.getInstance().displayStatus("Downloading " + filename + "...", UIController.mColorDarkGray);
                            }
                        });

                        boolean download = mFTPHandler.forceDownloadFile("Corporate", "Corporate", filename);

                        if (download) {
                            mCorporateProfile.locationDataUpdated = false;
                            mCorporateProfile.downloadCorporateLocation = false;


                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    FileSystem.getInstance().retrieveCorporateLocationData();
                                    LocationService.getInstance().computeDistance();
                                    UIController.getInstance().displayStatus(filename + " downloaded...", UIController.mColorGreen);

                                }
                            });

                        } else {
                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    UIController.getInstance().displayStatus(filename + " download failed...", UIController.mColorRed);
                                }
                            });
                        }
                    }
                    //UIController.getInstance().displayStatus(filename + " downloaded...", UIController.mColorGreen);

                    try {
                        Thread.sleep(mThreadWaitTime_CorporateLocation);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        mThread_CorporateFileDownload.start ();
    }

    public void initThread_DataFileDownload () {
        if (mThread_DataFileDownload != null) return;
        mThread_DataFileDownload = new Thread(new Runnable() {
            @Override
            public void run() {
                final FTPHandler mFTPHandler = FTPHandler.getInstance();
                final CorporateProfile mCorporateProfile = CorporateProfile.getInstance();

                while (true) {
                    while (FileSystem.mDownloadDataFiles) {
                        if (!mFTPHandler.isConnected()) continue;
                        /*
                        if (!mCorporateProfile.isLoggedOn) {
                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    UIController.getInstance().displayStatus("You are not logged in...", UIController.mColorRed);
                                }
                            });

                            continue;
                        }
                        */
                        UIHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                UIController.getInstance().displayStatus("Downloading files...", UIController.mColorDarkGray);
                            }
                        });

                        String localDirectory = "Data";
                        String workingDir = mFTPHandler.ftpGetCurrentWorkingDirectory();
                        String ftpDirectory = workingDir + "Files";
                        if (workingDir == null) {
                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    UIController.getInstance().displayStatus("Unable to retrieve working directory...", UIController.mColorDarkGray);
                                }
                            });
                        }
                        FTPFile[] ftpFiles = mFTPHandler.ftpPrintFilesList(ftpDirectory);
                        if (ftpFiles == null) {
                            UIHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    UIController.getInstance().displayStatus("No files exist on ftp directory...", UIController.mColorDarkGray);
                                }
                            });
                        } else {
                            String localDataDirectory = ApplicationContext.get().getApplicationInfo().dataDir;
                            FileSystem.getInstance().CreateDirectory(localDirectory);
                            int totalFiles = 0;
                            int totalDownloadedFiles = 0;

                            for (int i = 0, t = ftpFiles.length; i != t; ++i) {
                                final FTPFile  file = ftpFiles[i];
                                if (file.isFile()) {
                                    ++totalFiles;
                                    String downloadPath = localDataDirectory + "/" + localDirectory;
                                    File localFile = new File (downloadPath + "/" + file.getName());
                                    if (localFile.exists()) {
                                        localFile.delete();
                                    }

                                    UIHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            UIController.getInstance().displayStatus("Downloading " + file.getName() + "...", UIController.mColorDarkGray);
                                        }
                                    });

                                    boolean download = mFTPHandler.ftpDownload(ftpDirectory + "/" + file.getName(), downloadPath + "/" + file.getName());
                                    if (download) {
                                        ++totalDownloadedFiles;
                                        UIHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                UIController.getInstance().displayStatus(file.getName() + " downloaded", UIController.mColorGreen);
                                            }
                                        });
                                    } else {
                                        UIHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                UIController.getInstance().displayStatus("Downloading " + file.getName() + " failed...", UIController.mColorRed);
                                            }
                                        });
                                    }

                                }
                            }
                            boolean download = false;
                            if (totalDownloadedFiles != totalFiles) {
                                final int displayTotalDownloadedFiles = totalDownloadedFiles;
                                final int displayTotalFiles = totalFiles;
                                UIHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        UIController.getInstance().displayStatus("Only " + displayTotalDownloadedFiles + "/" + displayTotalFiles +  " were downloaded...", UIController.mColorRed);
                                    }
                                });
                            } else {
                                download = true;
                            }

                            if (download) {
                                updateListView = true;
                                FileSystem.mDownloadDataFiles = false;

                                FileSystem.getInstance().retrieveDataFiles();
                                UIHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        UIController.getInstance().invalidateListItems();
                                        UIController.getInstance().displayStatus("Data files download complete...", UIController.mColorGreen);
                                    }
                                });

                            }
                        }


                        try {
                            Thread.sleep(mThreadWaitTime_FileDownload);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        });
        mThread_DataFileDownload.start ();
    }


    public void destroyThreads () {

    }

}
