package com.amplestudio.datacrypt;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by myatphyothu on 23/9/17.
 */

public class UIController {

    private static UIController instance = null;
    AppCompatActivity m_MainActivity = null;

    SwipeMenuListView mySwipeMenuListView = null ;
    private ArrayList<DataObject> m_DataObjects = new ArrayList<DataObject> ();

    CustomAdapter m_Adapter = null;

    public static int mColorRed = Color.rgb(172,9,47);
    public static int mColorGreen = Color.rgb(0,168,3);
    public static int mColorDarkGray = Color.rgb(100,100,100);

    private UIController () {

    }

    public static UIController getInstance () {
        if (instance == null)
            instance = new UIController ();
        return instance;
    }

    public void init (AppCompatActivity _activity) {
        m_MainActivity = _activity;
    }

    public void initLocationTextFields () {
        TextView corporateLocation = (TextView) m_MainActivity.findViewById(R.id.textview_CorporateLocation);
        TextView corporateVicinity = (TextView) m_MainActivity.findViewById(R.id.textview_CorporateVicinity);
        CorporateProfile corporateProfile = CorporateProfile.getInstance();
        if (corporateProfile != null) {
            if (corporateLocation != null)
                corporateLocation.setText (corporateProfile.mLocation);

            if (corporateVicinity != null)
                corporateVicinity.setText (corporateProfile.mVicinity);
        }

        UIController.getInstance().displayUserStatus(CorporateProfile.getInstance().isLoggedOn ?
                CorporateProfile.getInstance().msg_LoggedOn : CorporateProfile.getInstance().msg_LoggedOff,
                CorporateProfile.getInstance().isLoggedOn ? UIController.mColorGreen : UIController.mColorRed);
    }

    public void addDataObjects (ArrayList<DataObject> _dataObjects) {
        m_DataObjects.clear ();
        for (DataObject dObject : _dataObjects) {
            m_DataObjects.add(dObject);
        }
    }


    public int totalDataObjects () { return m_DataObjects.size(); }

    public void displayContents () {

    }

    public void createSwipeListView () {
        if (instance == null) return;

        mySwipeMenuListView = (SwipeMenuListView) m_MainActivity.findViewById(R.id.mySwipeMenuListView);
        m_Adapter = new CustomAdapter (m_DataObjects);
        mySwipeMenuListView.setAdapter (m_Adapter);


        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(ApplicationContext.get());

                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                openItem.setWidth(170);
                openItem.setIcon (R.drawable.ic_action_view);
                menu.addMenuItem(openItem);

                SwipeMenuItem deleteItem = new SwipeMenuItem(ApplicationContext.get());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,0x3F, 0x25)));
                deleteItem.setWidth(170);
                deleteItem.setIcon(R.drawable.ic_action_delete);
                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        mySwipeMenuListView.setMenuCreator(creator);

        mySwipeMenuListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                DataObject dataObject = m_DataObjects.get(position);
                switch (index) {
                    case 0: // View file
                        decryptAndViewContents(position);
                        break;

                    case 1: // Delete
                        DeleteFile(position);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });


    }

    public void startMapActivity () {
        Intent intent = new Intent (ApplicationContext.get(), MapsActivity.class);
        intent.putExtra ("My Location", LocationService.getInstance().getUserLocation());
        intent.putExtra ("Radius", 100);
        m_MainActivity.startActivity (intent);
    }

    public void startMainActivity () {
        Intent intent = new Intent (ApplicationContext.get(), MainActivity.class);
        m_MainActivity.startActivity (intent);
    }

    public void startCorporateProfileActivity (boolean existing) {
        Intent intent = new Intent (ApplicationContext.get(), CorporateProfileActivity.class);
        intent.putExtra ("ProfileStatus", existing);
        m_MainActivity.startActivity (intent);
    }

    public void startActivity (Intent intent) {
        try {
            m_MainActivity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            alert ("Error", "Application for the file not found...", R.drawable.ic_error);
        }

    }

    private void startMapActivity (int index) {
        /*DataObject dataObject = m_DataObjects[index];
        Intent intent = new Intent (ApplicationContext.get(), Map.class);
        intent.putExtra (DataObject.tag_Filename, dataObject.getFilename());
        intent.putExtra ("Radius", 100);
        m_MainActivity.startActivity (intent);*/
    }

    public void invalidateListItems () {
        //m_Adapter.updateDataObjects(m_DataObjects);
        m_Adapter.setData (m_DataObjects);
        m_Adapter.notifyDataSetChanged();
        mySwipeMenuListView.invalidateViews();
    }

    private void viewEncryptedContents (int position) {
        DataObject dataObject = m_DataObjects.get(position);
        String encryptedContents = FileSystem.getInstance().getFileContents(ApplicationContext.get().getApplicationInfo().dataDir+"/Data/" + dataObject.getFilename());
        displayText (dataObject.getFilename(), encryptedContents);
    }

    private void decryptAndViewContents (int position) {
        LocationService.getInstance().computeDistance();
        if (LocationService.getInstance().mIsWithinVicinity) {
            promptPassword(position);

        }else
            alert("Not within vicinity", "You are " + LocationService.getInstance().mDistance + "km away from corporate.", R.drawable.ic_error);

    }

    public void promptPassword (final int position) {
        if (instance == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder (m_MainActivity);
        final EditText input = new EditText(m_MainActivity);
        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        builder.setView (input);

        builder.setPositiveButton ("Decrypt", new DialogInterface.OnClickListener(){
            @Override
            public void onClick (DialogInterface dialog, int which) {
                DecryptFileAndOpen(position, input.getText().toString());
            }
        });

        builder.setNegativeButton ("Cancel", new DialogInterface.OnClickListener() {
           @Override
           public void onClick (DialogInterface dialog, int which) {
                dialog.cancel();
           }
        });

        builder.setCancelable(false);
        builder.setTitle("Password?");
        builder.show();
    }

    private void DecryptFileAndOpen (int index, String inputPassword) {
        DataObject dataObject = m_DataObjects.get(index);
        byte[] decryptedContents = FileSystem.getInstance().decryptFile(ApplicationContext.get().getApplicationInfo().dataDir+ "/Data", dataObject.getFilename(), inputPassword);

        if (decryptedContents == null) {
            alert("File Decryption Error", "Invalid password!", R.drawable.ic_error);
            return;
        }
        if (dataObject.m_FileType == FILE_TYPE.FILE_TYPE_TEXT) {
            displayText (dataObject.getDecryptedFilename(), new String (decryptedContents));
        } else if (dataObject.m_FileType == FILE_TYPE.FILE_TYPE_IMAGE) {
            displayImage (dataObject.getDecryptedFilename(), decryptedContents);
        } else if (dataObject.m_FileType == FILE_TYPE.FILE_TYPE_PDF || dataObject.m_FileType == FILE_TYPE.FILE_TYPE_DOCX) {
            //String filePath = FileSystem.getInstance().writeDecryptedContentsToFile(dataObject.getDecryptedFilename(), decryptedContents);
            String filePath = FileSystem.getInstance().writeDecryptedContentsToFile (dataObject.getDecryptedFilename(), decryptedContents);
            if (filePath != null) {
                displayPDF (filePath);
            }
        }

    }

    private void DeleteFile (int index) {
        DataObject dataObject = m_DataObjects.get(index);
        FileSystem.getInstance().DeleteFile (ApplicationContext.get().getApplicationInfo().dataDir + "/Data/" + dataObject.getFilename());
        m_DataObjects.remove (index);
        m_Adapter.remove (index);

        invalidateListItems();
    }


    public void alert (String title, String message, int icon) {
        if (instance == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(m_MainActivity);
        builder.setCancelable(true);
        builder.setTitle(title);

        builder.setMessage(message);
        builder.setIcon (icon);
        builder.show();
    }


    // view data details for test purposes
    public void showDataObject (String title, DataObject dObj) {
        if (instance == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(m_MainActivity);
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append ("filename: " + dObj.getFilename() + "\n");
        stringBuffer.append ("extension: " + dObj.getFileExtension() + "\n");
        stringBuffer.append ("status: " + dObj.getFileStatus() + "\n");
        stringBuffer.append ("userlocation: " + dObj.getEncryptedLocation() + "\n");
        stringBuffer.append ("password: " + dObj.getFilePassword() + "\n\n");

        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(stringBuffer.toString());
        builder.show();
    }

    public void updateUserLocation () {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_UserLocation);
        if (textView == null) return;

        textView.setText (LocationService.getInstance().getUserLocation());
    }

    public void updateCorporateLocation (String location) {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_CorporateLocation);
        if (textView == null) return;

        textView.setText (location);
    }

    public void updateCorporateVicinity (String data) {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_CorporateVicinity);
        if (textView == null) return;

        textView.setText (data);
    }

    public void displayConnectionStatus (String data, int color) {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_ConnectionStatus);
        if (textView == null) return;


        textView.setTextColor (color);
        textView.setText (data);
    }

    public void displayUserStatus (String data, int color) {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_UserStatus);
        if (textView == null) return;

        textView.setTextColor (color);
        textView.setText (data);
    }

    public void displayStatus (String data, int color) {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_Status);
        if (textView == null) return;


        textView.setTextColor (color);
        textView.setText (data);
    }

    public void displayLocationStatus (String data, int color) {
        if (m_MainActivity == null) return;

        TextView textView = (TextView) m_MainActivity.findViewById(R.id.textview_DistanceDetails);
        if (textView == null) return;


        textView.setTextColor (color);
        textView.setText (data);
    }


    public void refereshSwipeListView () {
        if (m_DataObjects.size() == 0) {
            alert("No files", "Download the files from FTP server first...", R.drawable.ic_information);
            return;
        }

        if (ThreadController.getInstance().updateListView) {
            UIController.getInstance().displayStatus("", UIController.mColorDarkGray);
            invalidateListItems();
            ThreadController.getInstance().updateListView = false;
        } else {
            //alert("FTP download", "File download still in progress...", R.drawable.ic_information);
        }

    }

    public void displayText (String title, String message) {
        if (instance == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(m_MainActivity);
        builder.setCancelable(true);
        builder.setTitle(title);

        builder.setMessage(message);
        builder.show();
    }

    public void displayImage (String title, final byte[] imageBytes) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(m_MainActivity);

        //final AlertDialog dialog = builder.create();
        //LayoutInflater inflater = m_MainActivity.getLayoutInflater();
        //View dialogLayout = inflater.inflate(R.layout.image_viewer, null);
        dialog.setTitle(title);

        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //ImageView image = (ImageView) dialog.findViewById(R.id.imageview_alert_dialog_image_viewer);
        ImageView image = new ImageView (m_MainActivity);
        Bitmap bmp = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        image.setImageBitmap (bmp);
        //image.setImageBitmap(Bitmap.createScaledBitmap(bmp, image.getWidth(), image.getHeight(), false));

        float imageWidthInPX = (float)image.getWidth();


        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Math.round(imageWidthInPX),
                Math.round(imageWidthInPX * (float)bmp.getHeight() / (float)bmp.getWidth()));
        image.setLayoutParams(layoutParams);

        dialog.setView(image);


        dialog.create().show();
    }

    private void displayPDF(String filename) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(m_MainActivity);
        ImageView imageView = new ImageView(m_MainActivity);

        int REQ_WIDTH = 300;
        int REQ_HEIGHT = 800;

        Bitmap bitmap = Bitmap.createBitmap(REQ_WIDTH, REQ_HEIGHT, Bitmap.Config.ARGB_4444);
        File file = new File (filename);
        int currentPage = 0;

        if (file.exists()) {
            try {


                PdfRenderer renderer = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    renderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));
                }

                if (currentPage < 0) {
                    currentPage = 0;
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (currentPage > renderer.getPageCount()) {
                        currentPage = renderer.getPageCount() - 1;
                    }
                }

                Matrix m = imageView.getImageMatrix();
                Rect rect = new Rect(0, 0, REQ_WIDTH, REQ_HEIGHT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    renderer.openPage(currentPage).render(bitmap, rect, m, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                }
                imageView.setImageMatrix(m);
                imageView.setImageBitmap(bitmap);
                imageView.invalidate();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dialog.setView(imageView);

        dialog.create().show();
    }

}
