package com.amplestudio.datacrypt;

/**
 * Created by myatphyothu on 11/11/17.
 */

public class User {
    private String mFullName = "";
    private String mUsername = "";
    private String mPassword = "";

    public static final String USER_FULL_NAME = "NAME";
    public static final String USER_USERNAME = "USERNAME";
    public static final String USER_PASSWORD = "PASSWORD";

    static final int index_Fullname = 1;
    static final int index_Username = 2;
    static final int index_Password = 3;

    public User (String fullName, String username, String password) {
        set (fullName, username, password);
    }

    public void set (String fullName, String username, String password) {
        mFullName = fullName;
        mUsername = username;
        mPassword = password;
    }

    public String getFullName () { return mFullName; }
    public String getUsername () { return mUsername; }
    public String getPassword () { return mPassword; }
}
