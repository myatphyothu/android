package com.amplestudio.datacrypt;

import java.io.File;

/**
 * Created by myatphyothu on 24/8/17.
 */

public class Utility {
    private static final Utility ourInstance = new Utility();

    public static Utility getInstance() {
        return ourInstance;
    }

    private Utility() {
    }

    public static FILE_TYPE GetFileExtension (File file) {
        if (file == null) return FILE_TYPE.FILE_TYPE_ERROR;

        String filename = file.getName();
        String extension = filename.substring (filename.lastIndexOf('.')+1);

        if (extension.equalsIgnoreCase ("txt"))
            return FILE_TYPE.FILE_TYPE_TEXT;
        else if (extension.equalsIgnoreCase ("png") || extension.equalsIgnoreCase ("jpg")) // \ToDo Extend more image file types
            return FILE_TYPE.FILE_TYPE_IMAGE;
        else if (extension.equalsIgnoreCase ("pdf"))
            return FILE_TYPE.FILE_TYPE_PDF;
        else if (extension.equalsIgnoreCase ("mp4")) // \ToDo Extend more video file types
            return FILE_TYPE.FILE_TYPE_VID;
        else if (extension.equalsIgnoreCase ("mp3"))
            return FILE_TYPE.FILE_TYPE_AUDIO;
        else if (extension.equalsIgnoreCase ("docx"))
            return FILE_TYPE.FILE_TYPE_DOCX;
        else
            return FILE_TYPE.FILE_TYPE_ERROR;
    }


    public static FILE_TYPE GetFileExtension (String extension) {

        if (extension.equalsIgnoreCase ("txt"))
            return FILE_TYPE.FILE_TYPE_TEXT;
        else if (extension.equalsIgnoreCase ("png") || extension.equalsIgnoreCase ("jpg")) // \ToDo Extend more image file types
            return FILE_TYPE.FILE_TYPE_IMAGE;
        else if (extension.equalsIgnoreCase ("pdf"))
            return FILE_TYPE.FILE_TYPE_PDF;
        else if (extension.equalsIgnoreCase ("mp4")) // \ToDo Extend more video file types
            return FILE_TYPE.FILE_TYPE_VID;
        else if (extension.equalsIgnoreCase ("mp3"))
            return FILE_TYPE.FILE_TYPE_AUDIO;
        else if (extension.equalsIgnoreCase ("docx"))
            return FILE_TYPE.FILE_TYPE_DOCX;
        else
            return FILE_TYPE.FILE_TYPE_ERROR;
    }

    public static int GetIcon (FILE_TYPE type) {
        switch (type) {
            case FILE_TYPE_TEXT:
                return R.drawable.text;
            case FILE_TYPE_IMAGE:
                return R.drawable.image;
            case FILE_TYPE_PDF:
                return R.drawable.pdf;
            case FILE_TYPE_VID:
                return R.drawable.video;
            case FILE_TYPE_AUDIO:
                return R.drawable.audio;
            default:
                return R.drawable.unknown;

        }
    }

}
